// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@nuxt/ui'],
  app: {
    head: {
      title: 'Demolah',
      meta: [
        { charset: 'utf-8' },
        { name: 'description', content: 'Demolah'},
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/public/favicon.ico'},
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Material+Icons'}
      ]
    },
  }
})
